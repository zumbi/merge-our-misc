# vim: set sw=4 sts=4 et fileencoding=utf-8 :
#
# merge-our-misc configuration file, in Python syntax.

import yaml
import os
import glob

# Not directly used for anything, but we use them to reduce duplication
# in this file
_OBS_SERVER = "obs.example.com"
# MoM often runs on the same machine as the OBS master, but it can be
# different if you want. Again, this is not used by MoM, just used within
# this file.
_MOM_SERVER = _OBS_SERVER

# Output root. The default is /var/lib/merge-our-misc, but any directory
# with lots of disk space will do. The default in some existing installations
# was /srv/obs/merge-o-matic.
ROOT = "/var/lib/merge-our-misc"
ROOT_CONF = "/etc/merge-our-misc/"

# Website root
MOM_URL = "http://%s:83/" % _MOM_SERVER

# Used as the 'from' for any mails sent
MOM_NAME = "merge-our-misc"
MOM_EMAIL = "mom@%s" % _MOM_SERVER
MOM_SMTP_SERVER = "%s" % _MOM_SERVER

# Who to send bug emails to
RECIPIENTS = ['mom@%s' % _MOM_SERVER]

# Suffix to append to the package version after a merge
LOCAL_SUFFIX = "+local1"

# Distribution definitions
# For additional subprojects, use additional distro definitions
DISTROS = {}
for file in sorted(glob.iglob(os.path.join(ROOT_CONF, "distributions.d/*.yaml"))):
    with open(file,'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    DISTROS.update(cfg)

DISTRO_SOURCES = {}
for file in sorted(glob.iglob(os.path.join(ROOT_CONF, "sources.d/*.yaml"))):
    with open(file,'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    DISTRO_SOURCES.update(cfg)

DISTRO_TARGETS = {}

def defineDist(distro, name, upstreams, commitable,
        sources_per_package=None, local_suffix=LOCAL_SUFFIX):
  """Adds an entry to DISTRO_TARGETS.

     @param name The name of the distro
     @param upstream A key from DISTRO_SOURCES, linking this distro with a collection of upstream repos
     @param commitable Whether or not you want to be able to commit to OBS, or submit merge requests.
     @param distro The distro to use
  """
  if sources_per_package is None:
    sources_per_package = {}

  if local_suffix is None:
    local_suffix = {}

  for component in DISTROS[distro]['components']:
    DISTRO_TARGETS["%s-%s"%(name, component)] = {
      'distro': distro,
      'dist': name,
      'component': component,
      'sources': [ upstreams, ],
      'commit': commitable,
      'sources_per_package': sources_per_package,
# Debian packaging revision suffix for the first derived version; for instance
# Ubuntu's patched hello_1.2-3 package would be hello_1.2-3ubuntu1,
# hello_1.2-3ubuntu2, etc., so they would use "ubuntu1" in their
# MOM installation.     'sources_per_package': sources_per_package,
      'local_suffix': local_suffix,
    }

for file in sorted(glob.iglob(os.path.join(ROOT_CONF, "targets.d/*.yaml"))):
    with open(file,'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    for section, s in cfg.items():
        defineDist(s.get('distro'), s.get('name'), s.get('upstreams'), s.get('commitable'), s.get('sources_per_package'),s.get('local_suffix'))

# Time format for RSS feeds
RSS_TIME_FORMAT = "%a, %d %b %Y %H:%M:%S %Z"
