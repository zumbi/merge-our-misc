PACKAGE_NAME = merge-our-misc
VERSION ?= $(shell $(CURDIR)/get-version.sh)

SHELL = /bin/bash
PREFIX ?= /usr
PKGDATADIR ?= $(PREFIX)/share/$(PACKAGE_NAME)
OLDPKGLIBDIR = $(PREFIX)/lib/merge-o-matic
PYTHON ?= python

main_exe_files = \
	commit_merges.py \
	config.py \
	expire_pool.py \
	generate_diffs.py \
	generate_dpatches.py \
	generate_patches.py \
	get_missing_bases.py \
	grab-merge.sh \
	main.py \
	merge_report.py \
	merge_status.py \
	notify_action_needed.py \
	pack-archive.sh \
	produce_merges.py \
	publish_patches.py \
	stats_graphs.py \
	stats.py \
	update_sources.py

main_nonexe_files = \
	momlib.py

deb_nonexe_files = \
	deb/__init__.py \
	deb/controlfile.py \
	deb/source.py \
	deb/version.py

tmpl_nonexe_files = \
	templates/merge_report.html \
	$(NULL)

util_nonexe_files = \
	util/__init__.py \
	util/jinja2-AUTHORS \
	util/jinja.py \
	util/shell.py \
	util/tree.py

model_nonexe_files = \
	model/__init__.py \
	model/obs.py \
	model/debian.py \
	model/base.py \
	model/error.py

all_files = \
	$(main_exe_files) \
	$(main_nonexe_files) \
	$(deb_nonexe_files) \
	$(tmpl_nonexe_files) \
	$(util_nonexe_files) \
	$(model_nonexe_files) \
	COPYING \
	cron.d \
	Makefile \
	$(PACKAGE_NAME).logrotate \
	mom.conf \
	momsettings.py \
	README

all:

check:
	python -m tests

# We do not want to compile main.py
install: $(all_files)
	mkdir -p "$(DESTDIR)$(PKGDATADIR)"/{deb,templates,util,model}
	install -m 0644 $(main_nonexe_files) "$(DESTDIR)$(PKGDATADIR)"
	install -m 0755 $(main_exe_files) "$(DESTDIR)$(PKGDATADIR)"
	install -m 0644 $(deb_nonexe_files) "$(DESTDIR)$(PKGDATADIR)"/deb
	install -m 0644 $(tmpl_nonexe_files) "$(DESTDIR)$(PKGDATADIR)"/templates
	install -m 0644 $(util_nonexe_files) "$(DESTDIR)$(PKGDATADIR)"/util
	install -m 0644 $(model_nonexe_files) "$(DESTDIR)$(PKGDATADIR)"/model
	mkdir -p "$(DESTDIR)"/etc/$(PACKAGE_NAME)
	install -m 0644 momsettings.py "$(DESTDIR)"/etc/$(PACKAGE_NAME)
	echo "VERSION = '$(VERSION)'" > "$(DESTDIR)$(PKGDATADIR)/momversion.py"
	install -d $(DESTDIR)$(OLDPKGLIBDIR)
	set -e && for x in $(main_exe_files); do \
		ln -s $(PKGDATADIR)/$$x $(DESTDIR)$(OLDPKGLIBDIR)/$$x; \
	done

dist: $(PACKAGE_NAME)-$(VERSION).tar.bz2

$(PACKAGE_NAME)-$(VERSION).tar.bz2: $(all_files)
	-rm -r "$(PACKAGE_NAME)-$(VERSION)"
	mkdir -p "$(PACKAGE_NAME)-$(VERSION)"/{deb,templates,util,model}
	install -m 0644 \
		$(main_nonexe_files) \
		COPYING \
		cron.d \
		Makefile \
		$(PACKAGE_NAME).logrotate \
		mom.conf \
		momsettings.py \
		README \
		"$(PACKAGE_NAME)-$(VERSION)"
	install -m 0755 $(main_exe_files) "$(PACKAGE_NAME)-$(VERSION)"
	install -m 0644 $(deb_nonexe_files) "$(PACKAGE_NAME)-$(VERSION)"/deb
	install -m 0644 $(tmpl_nonexe_files) "$(PACKAGE_NAME)-$(VERSION)"/templates
	install -m 0644 $(util_nonexe_files) "$(PACKAGE_NAME)-$(VERSION)"/util
	install -m 0644 $(model_nonexe_files) "$(PACKAGE_NAME)-$(VERSION)"/model
	tar --format=ustar -chf - "$(PACKAGE_NAME)-$(VERSION)" | bzip2 -c > ""$(PACKAGE_NAME)-$(VERSION)".tar.bz2"
